-include Makefile

.PHONY: test package export godi wc reinstall

# De-installing and re-installing.

reinstall:
	$(MAKE) PREFIX=/usr/local uninstall install

# Test suite.

test: tool library
	$(MAKE) -s -C test/good $(MFLAGS)
	$(MAKE) -s -C test/bad $(MFLAGS)

# Package file creation.
# The version number is automatically set to the current date.

DATE    := $(shell /bin/date +%Y%m%d)
PACKAGE := alphaCaml-$(DATE)

package:
# Copier le r�pertoire courant (c'est-�-dire la working copy) vers /tmp.
	/bin/rm -rf /tmp/$(PACKAGE)
	/bin/cp -r `pwd` /tmp/$(PACKAGE)
# Nettoyer.
	$(MAKE) -C /tmp/$(PACKAGE) clean
# Copier le nouveau num�ro de version dans les fichiers ad hoc.
	echo version = \"$(DATE)\" >> /tmp/$(PACKAGE)/library/META
	echo let version = \"$(DATE)\" >> /tmp/$(PACKAGE)/tool/version.ml
	echo '\gdef\acversion{$(DATE)}' >> /tmp/$(PACKAGE)/doc/version.tex
# Construire la documentation.
	$(MAKE) -C /tmp/$(PACKAGE)/doc -rs clean pdf
	/bin/mv /tmp/$(PACKAGE)/doc/main.pdf /tmp/$(PACKAGE)/$(DOC)
# Cr�er l'archive.
	cd /tmp && tar -cvz --exclude-from $(PACKAGE)/.exclude -f $(PACKAGE).tar.gz $(PACKAGE)

RSYNC   := scp -p -B -C
TARGET  := yquem.inria.fr:public_html/alphaCaml/

export: package
# Copier l'archive et la doc vers yquem.
	(cd /tmp && $(RSYNC) $(PACKAGE).tar.gz $(TARGET))
	(cd /tmp/$(PACKAGE) && $(RSYNC) $(DOC) $(TARGET))
# Construire l'article et l'exporter vers yquem.
# (cd paper && ./export)

wc:
	ocamlwc -p tool/*.{ml,mll,mli,mly}
	ocamlwc -p library/*.{ml,mli}

# -------------------------------------------------------------------------

# Creating a GODI package.

# This entry assumes that "make package" and "make export" have been
# run on the same day. It must be run with sufficient rights to write
# into the local GODI hierarchy.

GODINAME := godi/godi-alphacaml
GODIWORK := /home/fpottier/dev/godi-build
GODISVN  := $(GODIWORK)/trunk/$(GODINAME)
GODIH    := $(GODI_HOME)/build/$(GODINAME)
GODIPACK := $(GODIWORK)/pack
GODIMAP  := $(GODIPACK)/release.3.09.map

godi:
	@ if [ `whoami` != "root" ] ; then \
	  echo "make godi must be run with root privileges." ; \
	  exit 1 ; \
	fi
	@ sed -e s/VERSION/$(DATE)/ < spec.godiva > $(GODISVN)/spec.godiva
	@ cd $(GODIWORK) && svn up
	@ cd $(GODISVN) && \
	  godiva -refetch -localbase $(GODI_HOME) spec.godiva && \
	  rsync -v -r $(GODIH)/ $(GODISVN) && \
	  chown -R fpottier.fpottier $(GODISVN)
	@ echo "Do you wish to proceed and commit changes to GODI (yes or no)?"
	@ read answer && if [ "$$answer" != "yes" ] ; then \
	  echo Aborting. ; \
	  exit 0 ; \
	fi
	@ cd $(GODISVN) && svn commit -m "Changes to alphaCaml package."
	@ echo "Now editing $(GODIMAP)..."
	@ echo "Please add or edit a line of the form trunk/$(GODINAME) <version number>."
	@ emacs $(GODIMAP)
	@ echo "Do you wish to proceed and commit changes to GODI (yes or no)?"
	@ read answer && if [ "$$answer" != "yes" ] ; then \
	  echo Aborting. ; \
	  exit 0 ; \
	fi
	@ cd $(GODIPACK) && svn commit -m "Updated release map for alphaCaml."
	@ echo "You may now open GODI's release tool at"
	@ echo "        https://gps.dynxs.de/godi_admin"
	@ echo "and proceed as directed."

