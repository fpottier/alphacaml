open MiniLinearFreshML

(* ------------------------------------------------------------------------- *)

(* Read the file and parse the program. *)

let prog : Identifier.t prog =
  let lexbuf = LexerUtil.open_in Sys.argv.(1) in
  try
    Parser.prog Lexer.main lexbuf
  with Parser.Error ->
    Error.errorb lexbuf "Syntax error."

(* Resolve all identifiers. *)

let prog : Atom.atom prog =
  I.prog prog

let () =
  Error.signaled()

(* Typecheck. *)

let () =
  check_prog prog

