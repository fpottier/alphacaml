module Var =
  Atom

(* ---------------------------------------------------------------------------- *)

(* Types. *)

type label =
    | LPositional of int
    | LNamed of string

type typ =
    | TTop
    | TVar
    | TOcc
    | TTuple of ltyp list
    | TSum of ltyp list
    | TAbstraction of typ

and ltyp =
    label * typ

(* ---------------------------------------------------------------------------- *)

(* Function types. *)

type ftyp = {
  
  domain: typ;

  codomain: typ;

}

(* ---------------------------------------------------------------------------- *)

(* Abstract syntax. *)

type var =
    Var.atom

type 'var lvar =
    label * 'var

type 'var value =
    | VTuple of 'var lvar list
    | VInjection of 'var lvar
    | VAbstraction of 'var * 'var

type 'var pattern =
    'var value (* patterns must be linear *)

type 'var expr =
    | EValue of 'var value
    | ECase of 'var * ('var pattern * 'var expr) list
    | ELet of 'var * 'var expr * 'var expr
    | EApp of 'var * 'var
    | ECopy of 'var
    | EDiscard of 'var
    | EDisassemble of 'var
    | EAssemble of 'var
    | EVar2Occ of 'var (* somewhat redundant, since one can also copy var's *)
    | EFresh

type 'var fundef =
    | FunDef of 'var * ftyp * 'var * 'var expr

type 'var prog =
    | Prog of 'var fundef list

(* ---------------------------------------------------------------------------- *)

(* Identifier resolution. *)

module I = struct

  open Import

  let rec var env x =
    resolve env x

  and lvar env (label, x) =
    (label, var env x)

  and lvars env lxs =
    List.map (lvar env) lxs

  and value env = function
    | VTuple lxs ->
	VTuple (lvars env lxs)
    | VInjection lx ->
	VInjection (lvar env lx)
    | VAbstraction (x1, x2) ->
	VAbstraction (var env x1, var env x2)

  and bvpat = function
    | VTuple lxs ->
	List.map snd lxs
    | VInjection (_, x) ->
	[ x ]
    | VAbstraction (x1, x2) ->
	[ x1; x2 ]

  and pattern env p =
    bind_simultaneously env (bvpat p)

  and expr env = function
    | EValue v ->
	EValue (value env v)
    | ECase (x, bs) ->
	ECase (var env x, branches env bs)
    | ELet (x, e1, e2) ->
	let env' = bind env x in
	ELet (var env' x, expr env e1, expr env' e2)
    | EApp (f, x) ->
	EApp (var env f, var env x)
    | ECopy x ->
	ECopy (var env x)
    | EDiscard x ->
	EDiscard (var env x)
    | EDisassemble x ->
	EDisassemble (var env x)
    | EAssemble x ->
	EAssemble (var env x)
    | EVar2Occ x ->
	EVar2Occ (var env x)
    | EFresh ->
	EFresh

  and branches env bs =
    List.map (branch env) bs

  and branch env (p, e) =
    let env' = pattern env p in
    value env' p, expr env' e

  and fundef env = function
    | FunDef (f, t, x, e) ->
	let env' = bind env x in
	FunDef (var env f, t, var env' x, expr env' e)

  and bvdef = function
    | FunDef (f, _, _, _) ->
	f

  and bvdefs defs =
    List.map bvdef defs

  and prog = function
    | Prog defs ->
	let env = bind_simultaneously empty (bvdefs defs) in
	Prog (List.map (fundef env) defs)

end

(* ---------------------------------------------------------------------------- *)

(* Environments. *)

type env = {

  equations: var value Var.AtomMap.t; (* a partial map *) (* for now, at most one equation per variable *)

  assignment: typ Var.AtomMap.t;  (* a total map *)

}

type fenv =
    ftyp Var.AtomMap.t (* a total map *)

(* ---------------------------------------------------------------------------- *)

(* Type-checking. *)

(* Introducing a new equation. *)

let intro_eq env x v =
  { env with equations = Var.AtomMap.add x v env.equations }

(* Introducing a new variable, or altering the type of an existing variable. *)

let intro_var env x t =
  { env with assignment = Var.AtomMap.add x t env.assignment }

(* [intro_pattern env p] checks that the pattern [p] is linear, and extends
   the context with bindings of the pattern's variables to [top]. *)

let intro_lvar env (_, x) =
  intro_var env x TTop

let intro_pattern env = function
  | VTuple lxs ->
      List.fold_left intro_lvar env lxs
  | VInjection (_, x) ->
      intro_var env x TTop
  | VAbstraction (x1, x2) ->
      intro_var (intro_var env x1 TTop) x2 TTop

(* [prune t] replaces all linear type constructors in [t] with [top]. So,
   [prune t] equals [t] if and only if [t] is a non-linear type. *)

(* TEMPORARY one could decide that [prune TTop] fails: values cannot be
   consumed twice. *)

let rec prune = function
  | TTop
  | TVar
  | TAbstraction _ ->
      TTop
  | TOcc ->
      TOcc
  | TTuple lts ->
      TTuple (prune_lts lts)
  | TSum lts ->
      TSum (prune_lts lts)

and prune_lts lts =
  List.map prune_lt lts

and prune_lt (label, t) =
  (label, prune t)

(* [nonlinear t] tells whether [t] is a non-linear type. *)

let rec nonlinear = function
  | TVar
  | TAbstraction _ ->
      false
  | TTop
  | TOcc ->
      true
  | TTuple lts
  | TSum lts ->
      nonlinear_lts lts

and nonlinear_lts lts =
  List.for_all nonlinear_lt lts

and nonlinear_lt (_, t) =
  nonlinear t

(* [equal t1 t2] makes sure that the types [t1] and [t2] are equal. *)

exception TypeMismatch of typ * typ

let rec equal t1 t2 =
  match t1, t2 with
  | TTop, TTop
  | TVar, TVar
  | TOcc, TOcc ->
      ()
  | TAbstraction t1, TAbstraction t2 ->
      equal t1 t2
  | TTuple lts1, TTuple lts2
  | TSum lts1, TSum lts2 ->
      equal_lts lts1 lts2
  | _, _ ->
      raise (TypeMismatch (t1, t2))

and equal_lts lts1 lts2 =
  List.iter2 equal_lt lts1 lts2

and equal_lt (_, t1) (_, t2) =
  equal t1 t2

let equalb t1 t2 =
  equal t1 t2;
  true

(* [no_Var t] requires that [t] contain no occurrence of [TVar]. *)

exception NoTVar

let rec no_TVar = function
  | TVar ->
      raise NoTVar
  | TTop
  | TOcc ->
      ()
  | TAbstraction t ->
      no_TVar t
  | TTuple lts
  | TSum lts ->
      List.iter no_TVar_lt lts

and no_TVar_lt (_, t) =
  no_TVar t

(* [typeof env x] returns the type associated with [x] in the environment. *)

let typeof env x : typ =
  try
    Var.AtomMap.find x env.assignment
  with Not_found ->
    assert false

(* [eqof env x] returns the equation associated with [x] in the environment. *)

exception NoEquation

let eqof env x : var value =
  try
    Var.AtomMap.find x env.equations
  with Not_found ->
    raise NoEquation

(* [contains v x] tells whether the value [v] contains a reference to the
   variable [x]. *)

let contains v x =
  match v with
  | VTuple lxs ->
      List.exists (fun (_, y) -> Var.equal x y) lxs
  | VInjection (_, y) ->
      Var.equal x y
  | VAbstraction (y1, y2) ->
      Var.equal x y1 || Var.equal x y2

(* [elim_var env x] removes the type assigned to [x] in the environment; this
   type must be non-linear, because implicit uses of [discard] are not
   allowed. Furthermore, any equations that mention [x] are removed. *)

exception ImplicitDiscard of var

let filter x eqs =
  Var.AtomMap.fold (fun y v eqs ->
    if contains v x then
      eqs
    else
      Var.AtomMap.add y v eqs
  ) eqs Var.AtomMap.empty

let remove x eqs =
  Var.AtomMap.remove x (filter x eqs)

let elim_var env x =
  let t = typeof env x in
  if nonlinear t then
    { equations = remove x env.equations;
      assignment = Var.AtomMap.remove x env.assignment }
  else
    raise (ImplicitDiscard x)

let elim_lvar env (_, x) =
  elim_var env x

let elim_pattern env p =
  match p with
  | VTuple lxs ->
      List.fold_left elim_lvar env lxs
  | VInjection (_, x) ->
      elim_var env x
  | VAbstraction (x1, x2) ->
      elim_var (elim_var env x1) x2

(* [consume_var env v] returns a pair of [v]'s current type in the environment
   and a new environment where the type of [v] has been updated (pruned) to
   reflect that it has been consumed. *)

let consume_var (env : env) (x : var) : typ * env =
  let t : typ = typeof env x in
  let env = intro_var env x (prune t) in
  t, env

let rec consume_lvars env lxs : ltyp list * env =
  match lxs with
  | [] ->
      [], env
  | (l, x) :: lxs ->
      let t, env = consume_var env x in
      let lts, env = consume_lvars env lxs in
      (l, t) :: lts, env

(* [join] reconciles an alternative between multiple environments at
   the exit of a case construct. *)

let conform env1 env2 : unit =
  let (b : bool) = Var.AtomMap.equal equalb env1.assignment env2.assignment in
  assert b

let join = function
  | [] ->
      assert false (* no case constructs with zero branches, for now *)
  | [ env ] ->
      env
  | env :: envs ->
      
      (* By construction, all branches must produce type assignments
	 with the same domain: the set of all variables that are in
	 scope. However, the types assigned to some variable could
	 differ. We check that they all agree. *)

      (* Ideally, all branches should produce identical sets of equations:
	 these should be exactly the equations that were valid at entry of
	 the case construct. *)

      (* In my current implementation, this is not true -- some equations
	 could be lost inside the case construct if a value is deconstructed
	 again -- but I ignore this detail for now. *)

      List.iter (conform env) envs;
      env

(* [disassemble] *)

exception Disassemble

let compare_lvs (label1, _) (label2, _) =
  Pervasives.compare label1 label2

let update_var env x t =
  let old : typ = typeof env x in
  assert (nonlinear old); (* not quite sure about this, but there should be no implicit discards *)
  intro_var env x t

let disassemble env v t : env =
  match v, t with
  | VTuple lxs, TTuple lts when List.length lxs = List.length lts ->
      let lxs = List.sort compare_lvs lxs
      and lts = List.sort compare_lvs lts in
      List.fold_left2 (fun env (_, x) (_, t) ->
	update_var env x t
      ) env lxs lts
  | VInjection (l, x), TSum lts when List.mem_assoc l lts ->
      let t = List.assoc l lts in
      update_var env x t
  | VAbstraction (x1, x2), TAbstraction t2 ->
      update_var (update_var env x1 TVar) x2 t2
  | _, _ ->
      raise Disassemble

(* [assemble] *)

exception Assemble

let assemble env v : typ * env =
  match v with
  | VTuple lxs ->
      let lts, env = consume_lvars env lxs in
      TTuple lts, env
  | VInjection (l, x) ->
      let t, env = consume_var env x in
      TSum [ (l, t) ], env (* TEMPORARY the other branches have unknown types *)
	                   (* this forces types to be reconciled at exist of case *)
	                   (* either do so or introduce named algebraic data types *)
  | VAbstraction (x1, x2) ->
      let t1, env = consume_var env x1 in
      let t2, env = consume_var env x2 in
      match t1 with
      | TVar ->
	  TAbstraction t2, env
      | _ ->
	  raise Assemble

(* [check_expr fenv env e x] checks expression [e] under environment [env],
   and under the assumption that the result of the expression is named [x]. It
   produces a new environment. *)

let rec check_expr (fenv : fenv) (env : env) (e : var expr) (x : var) : env =
  match e with
  | EValue v ->
      
      (* Binding [x] to [v] extends the context with the equation [x = v].
	 No type is assigned to [x], for now. [x] can later receive a type
	 via an [assemble] instruction. *)

      let env = intro_var env x TTop in
      let env = intro_eq env x v in
      env

  | ECase (y, branches) ->

      (* TEMPORARY could/should check that the case analysis is exhaustive
	 and irredundant. *)

      let t = typeof env y in

      let envs : env list =
	List.map (fun (p, e) ->

	  (* Matching [y] against [p] is permitted only if [p] matches [t]. Check this. *)
	  (* Matching [y] against [p] extends the context with the equation [y = p]. *)
	  (* Under this new context, we check [e] under the name [x]. *)
	  (* Then, we force [p] out of scope. *)

	  let env = intro_pattern env p in
	  let env = intro_eq env y p in
	  let _ = disassemble env p t in (* not effective now, but checks type compatibility *)
	  let env = check_expr fenv env e x in
	  let env = elim_pattern env p in
	  env

	) branches
      in

      (* Check that the environments produced by the various branches agree. *)

      join envs

  | ELet (y, e1, e2) ->

      (* Sequencing. *)

      (* Check [e1] under the name [y]. Check [e2] under the name [x]. Then,
	 force [y] out of scope. *)

      let env = check_expr fenv env e1 y in
      let env = check_expr fenv env e2 x in
      let env = elim_var env y in
      env

  | EApp (f, y) ->

      (* [x] is being bound to [f y]. *)

      (* Look up the type of [f]. *)

      let ft : ftyp =
	try
	  Var.AtomMap.find f fenv
	with Not_found ->
	  assert false
      in

      (* Check that its domain matches the type of [y]. *)
      
      equal ft.domain (typeof env y);

      (* Consume [y], and introduce [x]. *)

      let env = intro_var env y TTop in
      let env = intro_var env x ft.codomain in
      env

  | ECopy y ->

      (* [x] is being bound to [copy y]. *)

      (* Look up the type of [y] and create an identical binding for [x]. *)

      (* If this type contains any occurrence of [TVar], fail. Copying at
	 type [TVar] could make sense -- it just creates a fresh copy of
	 the binder, yielding a distinct binder -- but that semantics does
	 not coincide with the semantics of FreshML after [copy] is erased. *)

      let t = typeof env y in
      no_TVar t;
      let env = intro_var env x t in
      env

  | EDiscard y ->

      (* [x] is being bound to [discard y]. *)

      (* Both [x] and [y] should become bound to [top]. *)

      let env = intro_var env x TTop in
      let env = intro_var env y TTop in
      env

  | EDisassemble y ->

      (* [x] is being bound to [disassemble y]. *)

      (* Look for a type [y : t] and an equation [y = v]. *)

      let t = typeof env y
      and v = eqof env y in

      (* Remove the binding [y : t]. *)

      let env = intro_var env y TTop in

      (* Use the equation to disassemble the type. This updates the
	 types of the free variables of [v]. *)

      let env = disassemble env v t in

      (* Bind [x] to [top], and continue. *)

      let env = intro_var env x TTop in
      env

  | EAssemble y ->

      (* [x] is being bound to [assemble y]. *)

      (* Look for an equation [y = v]. *)

      let v = eqof env y in

      (* Steal ownership of the components of [v], and transfer it onto
	 [y]. *)

      let t, env = assemble env v in
      let env = intro_var env y t in
   
      (* Bind [x] to [top], and continue. *)

      let env = intro_var env x TTop in
      env

  | EVar2Occ y ->

      (* [x] is being bound to [var2occ y]. *)

      begin match typeof env y with
      | TVar ->
	  intro_var env x TOcc
      | t ->
	  raise (TypeMismatch (TVar, t))
      end

  | EFresh ->

      (* [x] is being bound to [fresh]. *)

      let env = intro_var env x TVar in
      env

(* Typechecking function definitions. *)

let check_fundef fenv (FunDef (f, { domain = t1; codomain = t2 }, x, e)) =
  (* Build an entry environment. *)
  let env : env = {
    equations = Var.AtomMap.empty;
    assignment = Var.AtomMap.add x t1 Var.AtomMap.empty 
  } in
  (* Obtain an exit environment. *)
  let y = Atom.fresh (Atom.identifier f) in
  let env = check_expr fenv env e y in
  (* Check that [x] has been consumed and that [y] has the expected type [t2]. *)
  if nonlinear (typeof env x) then
    raise (ImplicitDiscard x);
  equal (typeof env y) t2

(* Typechecking whole programs. *)

let fenv defs : fenv =
  List.fold_left (fun fenv (FunDef (f, t, _, _)) ->
    Var.AtomMap.add f t fenv
  ) Var.AtomMap.empty defs

let check_prog (Prog defs) =
  let fenv = fenv defs in
  List.iter (check_fundef fenv) defs

