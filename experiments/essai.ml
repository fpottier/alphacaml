open Signatures

(* TEMPORARY manque la m'emoisation *)

module Make (T : Toolbox) = struct

  open T

  type 'a nominal = <
    support: AtomSet.t;
    apply: Subst.subst -> 'a nominal;
    deconstruct: 'a;
  >

  type term =
      raw_term nominal

  and raw_term =
      | Var of Atom.t
      | Abs of Atom.t * term
      | App of term * term

  class var (x : Atom.t) =
    object
      method support = AtomSet.singleton x;
      method apply subst = new var (Subst.lookup x subst);
      method deconstruct = Var x;
    end

  let mkvar x : term =
    new var x

  class app (t1 : term) (t2 : term) =
    object 
      method support = AtomSet.union t1#support t2#support;
      method apply subst = new app (t1#apply subst) (t2#apply subst);
      method deconstruct = App (t1, t2);
    end

  let mkapp t1 t2 : term =
    new app t1 t2

  class abs (x : Atom.t) (t : term) =
    object
      method support = AtomSet.remove x t#support;
      method apply subst = new abs x (t#apply (Subst.remove x subst));
      method deconstruct =
	let x' = Atom.fresha x in
	Abs (x', t#apply (Subst.singleton x x'))
    end

  let mkabs x t : term =
    new abs x t

  let rec traverse (t : term) =
    match t#deconstruct with
    | Var _ ->
	()
    | Abs (_, t) ->
	traverse t
    | App (t1, t2) ->
	traverse t1;
	traverse t2

end

