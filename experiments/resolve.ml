module GenericPart
  (Identifier : sig
     type t = Atoms.info (* TEMPORARY *)
     val compare: t -> t -> int
   end) =
struct

  module M = Map.Make (Identifier)

  type env =
      Atoms.atom M.t

end

open MiniLinearFreshML

