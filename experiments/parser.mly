%token<Identifier.t> LIDENT
%token<string> UIDENT
%token LPAR RPAR COMMA LANGLE RANGLE SEMI BAR ARROW EOF DEFEQ PLUS COLON
%token CASE OF END LET IN FUN
%token COPY DISCARD DISASSEMBLE ASSEMBLE VAR2OCC FRESH TOP VAR OCC

%start<Identifier.t MiniLinearFreshML.prog> prog

%{

  open MiniLinearFreshML

  let rec number i xs =
    match xs with
    | [] ->
	[]
    | x :: xs ->
	(LPositional i, x) :: number (i + 1) xs

  let wildcard : Identifier.t =
    Identifier.make "_" (0, "") Lexing.dummy_pos Lexing.dummy_pos

%}

%%

(* ------------------------------------------------------------------------- *)

(* This parameterized non-terminal symbol recognizes separated lists of at
   least two elements -- which means that at least one separator is
   explicit. The definition is inlined, so as to make the first separator
   visible at the instantiation site. *)

%inline separated_list_of_two_or_more(separator, X):
| head = X separator tail = separated_nonempty_list(separator, X)
    { head :: tail }

parenthesized_tuple(X):
| LPAR xs = separated_list_of_two_or_more(COMMA, X) RPAR
    { xs }

parenthesized_sum(X):
| LPAR xs = separated_list_of_two_or_more(PLUS, X) RPAR
    { xs }

typ:
| TOP
    { TTop }
| VAR
    { TVar }
| OCC
    { TOcc }
| ts = parenthesized_tuple(typ)
    { TTuple (number 0 ts) }
| lts = parenthesized_sum(ltyp)
    { TSum lts }
| LANGLE t = typ RANGLE
    { TAbstraction t }

ltyp:
| k = UIDENT t = typ
    { LNamed k, t }

value:
| xs = parenthesized_tuple(LIDENT)
    { VTuple (number 0 xs) }
| k = UIDENT x = LIDENT
    { VInjection (LNamed k, x) }
| LANGLE x = LIDENT RANGLE y = LIDENT
    { VAbstraction (x, y) }

expression0:
| v = value
    { EValue v }
| CASE x = LIDENT OF bs = branch+ END
    { ECase (x, bs) }
| f = LIDENT x = LIDENT
    { EApp (f, x) }
| COPY x = LIDENT
    { ECopy x }
| DISCARD x = LIDENT
    { EDiscard x }
| DISASSEMBLE x = LIDENT
    { EDisassemble x }
| ASSEMBLE x = LIDENT
    { EAssemble x }
| VAR2OCC x = LIDENT
    { EVar2Occ x }

expression1:
| e = expression0
    { e }
| LET x = LIDENT DEFEQ e1 = expression IN e2 = expression1
    { ELet (x, e1, e2) }
| LET p = value DEFEQ x = LIDENT IN e2 = expression1
    { ECase (x, [ p, e2 ]) }
| FRESH x = LIDENT IN e = expression1
    { ELet (x, EFresh, e) }
| e1 = expression0 SEMI e2 = expression1
    { ELet (wildcard, e1, e2) }

%inline expression:
| e = expression1
    { e }

branch:
| BAR p = value ARROW e = expression
    { p, e }

fundef:
| FUN f = LIDENT LPAR x = LIDENT COLON t1 = typ RPAR COLON t2 = typ DEFEQ e = expression
    { FunDef (f, { domain = t1; codomain = t2 }, x, e) }

prog:
| defs = fundef* EOF
    { Prog defs }

