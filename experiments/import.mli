(* This module provides support for mapping identifiers to atoms. *)

(* Errors about undefined or multiply-defined identifiers are reported
   on [stderr], via the [Error] module. An entire phase can be carried
   out even if errors are found on the way. At the end of the phase,
   it is up to the client to invoke [Error.signaled()], so that,
   if any errors were found, the program is aborted. *)

(* An import environment maps identifiers to atoms. *)

type env

(* The empty environment. *)

val empty: env

(* Contanenation of environments. The bindings in the right-hand
   environment take precedence. *)

val cat: env -> env -> env

(* [fragment xs] turns a list of identifiers, which are to be newly
   bound, into an environment fragment that maps each identifier in
   [xs] to a fresh atom. A check against multiply-defined identifiers
   is performed. The power of this operation can be necessary for
   certain complex binding structures; however, in most common cases,
   the [bind] functions below, which extend a pre-existing
   environment, are sufficient. *)

val fragment: Identifier.t list -> env

(* [linear xs] checks the list [xs] against multiply-defined identifiers. *)

val linear: Identifier.t list -> unit

(* [bind_simultaneously env xs] extends the pre-existing environment
   [env] with the new environment [fragment xs]. *)

val bind_simultaneously: env -> Identifier.t list -> env
  
(* [bind env x] is equivalent to [bind_simultaneously env [x]]. *)

val bind: env -> Identifier.t -> env

(* [bind_sequentially env xs] binds the sequence of identifiers [xs],
   one after the other. *)

val bind_sequentially: env -> Identifier.t list -> env

(* [resolve env x] looks up the identifier [x] in the environment [env].
   If [x] is unknown, an error is signaled. *)

val resolve: env -> Identifier.t -> Atom.atom

(* Sometimes, identifiers of a certain sort are bound globally and implicitly
   -- that is, there is no explicit binding form for them. In that case, a
   mutable, global environment must be used to map identifiers to atoms. The
   call [mkglobal()] creates such a global environment, and returns a function
   that turns identifiers into atoms. *)

val mkglobal: unit -> (Identifier.t -> Atom.atom)

