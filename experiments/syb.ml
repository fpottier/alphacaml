(* TEMPORARY

   songer a un style oriente objet plus classique, ou les methodes sont
   directement des fold, et pas des fold transformers

   comment gerer les applications de types si les deux types ont des
   tags differents? il faudrait fusionner les deux objets dictionnaire
   en un seul, et de meme au niveau de leurs types. Ca pourrait marcher
   si le descripteur associe aux listes (par exemple) est de type

     ('tag, 'a) descriptor -> ('tag, 'a list) descriptor
       constraint 'tag = < list: ... >

   ??

*)

(* An ocaml object plays the role of a typed, heterogeneous dictionary. *)

type base =
    | Bunit
    | Bbool
    | Bint
    | Bnativeint
    | Bint32
    | Bint64
    | Bchar
    | Bstring
    | Bfloat
    | Bexn

type tag =
    Obj.t -> Obj.t (* really a method selector, of the form (fun o -> o#tag) *)

type sum_descriptor = {

  (* For each constructor of arity zero, a tag. *)

  constant_constructors: tag array;

  (* For each constructor of non-zero arity, a pair of a tag and a tuple
     descriptor. *)

  nonconstant_constructors: (tag * tuple_descriptor) array;

}

and tuple_descriptor =

  (* For each field, a pair of a tag and a descriptor. *)

  (tag * descriptor) array

and descriptor =
    | DOpaque
    | DBase of base
    | DSumTuple of sum_descriptor
    | DArray of descriptor
    | DTag of tag * descriptor

let identity_fold accu data =
  accu

type ('accu, 'data) fold =
    'accu -> 'data -> 'accu

type ('accu, 'data) fold_transformer =
    ('accu, 'data) fold -> ('accu, 'data) fold

let rec fold
    (transform : Obj.t) (* a dictionary object, roughly tag -> ('accu, Obj.t) fold_transformer *)
    (desc : descriptor)
    (accu : 'accu)
    (data : Obj.t)
    : 'accu =
  match desc with
  | DOpaque
  | DBase _ ->
      accu
  | DSumTuple sum_desc ->
      if Obj.is_int data then begin
	let b = (Obj.magic data : int) in
	assert (0 <= b && b < Array.length sum_desc.constant_constructors);
	let tag = sum_desc.constant_constructors.(b) in
	(Obj.magic (tag transform) : ('accu, Obj.t) fold_transformer) identity_fold accu data
      end
      else begin
	assert (Obj.is_block data);
	let b = Obj.tag data in
	assert (0 <= b && b < Array.length sum_desc.nonconstant_constructors);
	let tag, tuple_desc = sum_desc.nonconstant_constructors.(b) in
	(Obj.magic (tag transform) : ('accu, Obj.t) fold_transformer) (fun accu data ->
	  let n = Obj.size data in
	  assert (0 < n && n = Array.length tuple_desc);
	  let rec loop i accu =
	    if i = n then
	      accu
	    else
	      let tag, desc = tuple_desc.(i) in
	      loop (i+1) (
		(Obj.magic (tag transform) : ('accu, Obj.t) fold_transformer) (fold transform desc) accu (Obj.field data i)
	      )
	  in
	  loop 0 accu
	) accu data
      end
  | DArray desc ->
      let data = (Obj.magic data : Obj.t array) in
      Array.fold_left (fold transform desc) accu data
  | DTag (tag, desc) ->
      (Obj.magic (tag transform) : ('accu, Obj.t) fold_transformer) (fold transform desc) accu data

type terme =
    | Var
    | Abs of terme
    | App of terme * terme

let var o = o#var
let var = (Obj.magic var : tag)
let abs o = o#abs
let abs = (Obj.magic abs : tag)
let app o = o#app
let app = (Obj.magic app : tag)
let field o = o#field
let field = (Obj.magic field : tag)
let terme o = o#terme
let terme = (Obj.magic terme : tag)


let rec __desc_terme : descriptor =
  DTag (terme,
    DSumTuple {
      constant_constructors = [| var |];
      nonconstant_constructors = [| abs, [| field, __desc_terme |]; app, [| field, __desc_terme; field, __desc_terme |] |];
    }
  )

class type ['accu] fold_dictionary = object
  method var: ('accu, terme) fold_transformer
  method abs: ('accu, terme) fold_transformer
  method app: ('accu, terme) fold_transformer
  method field: ('accu, terme) fold_transformer
  method terme: ('accu, terme) fold_transformer
end

let fold_terme : 'accu fold_dictionary -> ('accu, terme) fold =
  fun transform accu data ->
    fold (Obj.repr transform) __desc_terme accu (Obj.repr data)

class ['accu] transform = object
  method var: ('accu, terme) fold_transformer =
    fun fold -> fold
  method abs: ('accu, terme) fold_transformer =
    fun fold -> fold
  method app: ('accu, terme) fold_transformer =
    fun fold -> fold
  method field: ('accu, terme) fold_transformer =
    fun fold -> fold
  method terme: ('accu, terme) fold_transformer =
    fun fold -> fold
end

let count_subterms = object
  inherit [int] transform
  method terme: (int, terme) fold_transformer =
    fun fold -> fun accu data -> fold (accu + 1) data
end

let t : terme =
  Abs (App (Abs Var, Abs (Abs Var)))

let c : int =
  fold_terme
    count_subterms
    0
    t

let () =
  Printf.printf "%d\n%!" c

