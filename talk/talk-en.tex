%; whizzy section -advi "advi -geometry 1270x1024 -fullwidth -html Start-Document"
\documentclass[compress,professionalfont,xcolor={dvipsnames}]{beamer}
\usepackage{amsmath}
\usepackage{beamerthemesplit}
\usepackage{infomath}
\usepackage[latin1]{inputenc}
\usepackage{listings}
\usepackage{xspace}
\input{talk-setup}
\input{../paper/macros}
\usepackage{../paper/mymacros}
\renewcommand{\ac}{alphaCaml\xspace}

% ----------------------------------------------------------------------------
% Page de titre.

\pgfdeclareimage[height=5mm]{logo}{images/Logo-INRIA-couleur}
\titlegraphic{\pgfuseimage{logo}}
\title{An overview of \ac}
\author{Fran�ois Pottier}
\date{September 2005}

\begin{document}
\frame{\titlepage}

% ----------------------------------------------------------------------------

\section{Introduction}

\frame{
\frametitle{Motivation}

Our programming languages do not support \insist{abstract syntax
with binders} in a satisfactory way.
%
% Les d�clarations de types alg�briques permettent de d�crire la
% structure de termes du premier ordre, mais ne mod�lisent pas
% la notion de liaison.



\insist{Hand-coding} the operations that deal with lexical scope
(capture-avoiding substitution, etc.) is tedious and
error-prone.



How about a more \insist{declarative},
\insist{robust}, \insist{automated} approach?

\hskip 3cm -- cf.\ Shinwell's Fresh O'Caml, Cheney's FreshLib.

}

\frame{
\frametitle{Three facets}

Let's distinguish three facets of the problem:
\begin{itemize}
\item a \insist{specification language},
\item an \insist{implementation technique},
\item an \insist{automated translation} of the former to the latter.
\end{itemize}
In this talk, I emphasize the first aspect.

}

% ----------------------------------------------------------------------------

\section{A specification language}

\frame{
\frametitle{Prior art}

There have been a few proposals to enrich algebraic specification
languages with \insist{names} and \insist{abstractions}.

An abstraction usually takes the form $\hilite{\langle\at\rangle\ex}$, or
$\hilite{\langle \at_1, \ldots, \at_n\rangle\ex}$, or, as in Fresh
Objective Caml, $\hilite{\langle\ex_1\rangle\ex_2}$.

Abstraction is always \insist{binary:} the names (or \insist{atoms}) $\at$
that appear on the left-hand side are bound, and their scope is the
expression $\ex$ that appears on the right-hand side.

}

\frame[containsverbatim]{
\frametitle{Example: pure $\lambda$-calculus}

Pure $\lambda$-calculus:
%
$$\lt := \at \mid \lt\,\lt \mid \lambda\at.\lt$$
%
is modelled in \freshocaml as follows:
%
\begin{lstlisting}
    bindable_type var

    type term =
      | EVar of var
      | EApp of term * term
      | ELam of $\!\langle$var$\rangle$term
\end{lstlisting}

}

\frame[containsverbatim]{
\frametitle{A more delicate example}

Let's add \insist{simultaneous} definitions:
%
$$\lt ::= \ldots \mid \ekw{let}\; \at_1=\lt_1 \;\ekw{and}\; \ldots \;\ekw{and}\; \at_n=\lt_n \;\ekw{in}\; \lt$$
%
The atoms $\at_i$ are bound, so they must lie \insist{within} the
abstraction's left-hand side. The terms $\lt_i$ are outside the abstraction's
lexical scope, so they must lie \insist{outside} of the abstraction:
%
\begin{lstlisting}
    type term =
      | ...
      | ELet of term list * $\!\langle$var list$\rangle$term
\end{lstlisting}
%
% Codage peu fid�le � la d�finition initiale, et fragile,
% puisque les deux listes peuvent � pr�sent �tre de longueurs distinctes. Junk.

}

\frame[containsverbatim]{
\frametitle{Another delicate example}

Simultaneous \insist{recursive} definitions pose a similar problem:
%
$$\lt ::= \ldots \mid \ekw{let rec}\; \at_1=\lt_1 \;\ekw{and}\; \ldots \;\ekw{and}\; \at_n=\lt_n \;\ekw{in}\; \lt$$
%
The terms $\lt_i$ are now inside the abstraction's lexical scope,
so they must lie within the abstraction's \insist{right-hand} side:
%
\begin{lstlisting}
    type term =
      | ...
      | ELetRec of $\!\langle$var list$\rangle$(term list * term)
\end{lstlisting}
%
% M�me probl�me.

}

\frame{
\frametitle{The problem}

The root of the problem is the assumption that \insist{lexical} and
\insist{physical} structure should coincide.

}

\frame{
\frametitle{A solution}

Within an abstraction, \ac distinguishes three basic components:
\insist{binding occurrences} of names, expressions that lie \insist{within} the
abstraction's lexical scope, and expressions that lie \insist{outside} the scope.

These components are assembled using sums and products, giving rise to a
syntactic category of so-called \insist{patterns}. Abstraction becomes
\insist{unary} and holds a pattern.
%
\[\begin{array}{r@{{}::={}}l@{\qquad}l}
\et & \tunit \mid \et\times\et \mid \et+\et \mid \hilite\atom \mid \hilite{\abs\pt}
& \textit{Expression types} \\
\pt & \tunit \mid \pt\times\pt \mid \pt+\pt \mid \hilite\atom \mid \hilite{\modapp\minner\et}
                                                              \mid \hilite{\modapp\mouter\et}
& \textit{Pattern types}
\end{array}\]
%
% La d�finition des types est � lire de mani�re co-inductive.
%
% D�finition en termes d'une seule sorte d'atomes.
%
% Une specification \ac est essentiellement la donn�e d'une famille de types
% d'expression et de motif.
%
% On peut d�finir formellement la relation d'alpha-�quivalence associ�e
% � un type.
%

}

\frame[containsverbatim]{
\frametitle{Back to pure $\lambda$-calculus}

Pure $\lambda$-calculus is modelled in \ac as follows:
%
\begin{lstlisting}
    sort var

    type term =
      | EVar of atom var
      | EApp of term * term
      | ELam of $\!\langle$lamp$\rangle$

    type lamp binds var =
        atom var * inner term
\end{lstlisting}

}

\frame[containsverbatim]{
\frametitle{A second look at simultaneous definitions}

Simultaneous definitions are modelled without difficulty:
%
\begin{lstlisting}
    type term =
      | ...
      | ELet of $\!\langle$letp$\rangle$

    type letp binds var =
        binding list * inner term

    type binding binds var =
        atom var * outer term
\end{lstlisting}
%
% Un peu verbose car on est oblig� de nommer certains types interm�diaires.
%
% Il en va de m�me de $\ekw{let rec}$.

}

\frame[containsverbatim]{
\frametitle{More advanced examples}

Abstract syntax for patterns in an \ocaml-like programming language
could be declared like this:
%
\begin{lstlisting}
    type pattern binds var =
      | PWildcard
      | PVar of atom var
      | PRecord of pattern StringMap.t
      | PInjection of [ constructor ] * pattern list
      | PAnd of pattern * pattern
      | POr of pattern * pattern
\end{lstlisting}
%
% Lorsqu'une valeur de type \lstinline|pattern| appara�t au sein
% d'une abstraction, tous les noms situ�s aux feuilles \lstinline|PVar|
% sont consid�r�s comme liants.
%
% Un nom peut avoir de multiples occurrences liantes.
%
% Les constructeurs de types StringMap.t et list doivent �tre
% connus d'alphaCaml (map, fold).
%
% Les crochets permettent de mentionner un type \ocaml arbitraire.
%
% Pas de conditions de bonne formation sur PAnd/POr.
%
}

% ----------------------------------------------------------------------------

\section{Implementation techniques}

\frame{
\frametitle{Three known techniques}

\begin{enumerate}
\item \insist{de Bruijn} indices. Require \insist{shifting}, which is fragile.
      No freshening. Generic equality and hashing functions respect $\alpha$-equivalence.
\item \label{atoms} \insist{Atoms}. Require \insist{freshening} upon opening abstractions.
      No shifting. Require custom equality and hashing functions.
\item \label{pollack} \insist{Pollack mix:} free names as atoms and bound names as indices. Analogous to~\ref{atoms},
      except generic equality and hashing respect $\alpha$-equivalence.
\end{enumerate}

\ac follows~\ref{atoms}.

}

\frame{
\frametitle{Some more details}

Atoms are represented as pairs of an integer and a string. The
latter is used only as a hint for display.

Sets of atoms and renamings are encoded as Patricia trees.

Renamings are \insist{suspended} and \insist{composed} at
abstractions, which allows linear-time term traversals.

Even though the fresh atom generator has state, \insist{closed} terms can
safely be marshalled to disk.

%
% On pourrait �galement employ�e la technique mixte o� noms li�s sont
% repr�sent�s par des indices de de Bruijn et noms libres par des atomes.
%
% Pas d'innovation particuli�re.

}

% ----------------------------------------------------------------------------

\section{Translating specifications}

\frame[containsverbatim]{
\frametitle{Types}

The specification of pure $\lambda$-calculus is translated
down to \ocaml as follows. Atoms and abstractions are \insist{abstract}.
%
\begin{lstlisting}
    type var = Var.Atom.t

    type term =
      | EVar of var
      | EApp of term * term
      | ELam of opaque_lamp

    and lamp =
        var * term

    and opaque_lamp
\end{lstlisting}
%
% Chaque sorte de noms donne lieu � un type abstrait distinct.
% Impossible d'examiner une abstraction sans la rafra�chir.

}

\frame[containsverbatim]{
\frametitle{Code}

Opening an abstraction automatically \insist{freshens} its bound
atoms.
%
\begin{lstlisting}
    val open_lamp: opaque_lamp -> lamp
    val create_lamp: lamp -> opaque_lamp
\end{lstlisting}
%
This enforces Barendregt's informal convention.

More boilerplate is \insist{generated} for computing sets of free or bound
atoms, applying renamings, helping clients succinctly define transformations
(such as capture-avoiding substitution), etc.

}

% ----------------------------------------------------------------------------

\section{Conclusion}

\frame{
\frametitle{Status}

\ac is \insist{available}. There are very few known users so far.



The distribution comes with \insist{two demos}:
\begin{itemize}
\item a na�ve typechecker and evaluator for $F_\leq$
\item a na�ve evaluator for a calculus of mixins (Hirschowitz \etal.)
\end{itemize}
These limited experiments are encouraging.

}

\frame{
\frametitle{Limitations}

One \insist{must} go through \textit{open} functions to examine
abstractions. \insist{Deep pattern matching} is impossible.

Clients can write \insist{meaningless} code, such as a function
that pretends to collect the bound atoms in an expression.
%
% Pour �viter cela, il faudrait que \lstinline|open|, au lieu de renvoyer
% des noms frais sans pouvoir contr�ler l'usage qui en sera fait, attende
% une continuation et exige une preuve que cette continuation ne laissera
% pas �chapper les noms frais.
%

}

\frame{
\frametitle{Towards alpha-(your-favorite-prover-here)?}

How about translating a specification language like \ac's into
\insist{theorems} (recursion and induction principles) and \insist{proofs?}

\hskip 4cm -- cf.\ Pitts, Urban and Tasson, Norrish...
%
% Mais je sors de mon domaine d'expertise...
%

}

\end{document}

