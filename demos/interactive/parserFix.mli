(* A reference to a function that reads and parses more input.
   This reference is initialized in [Main] and read in [Parser]. *)

val declarations: (unit -> Syntax.Raw.declarations) ref

