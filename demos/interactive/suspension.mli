(* Suspensions are delayed computations.

   When a suspension is forced, the underlying computation is run,
   producing a result that is returned.

   No memoization is performed. The basic assumption is that
   computations are impure. Running a computation twice produces
   different outcomes, so memoization does not make any sense.

   The definition of [map] is canonical, while [fold] and [fold2] are
   currently unimplemented. *)

type 'a t

val create: (unit -> 'a) -> 'a t
val force: 'a t -> 'a
val map: ('a -> 'b) -> 'a t -> 'b t
val fold: ('b -> 'a -> 'b) -> 'b -> 'a t -> 'b
val fold2: ('b -> 'a -> 'a -> 'b) -> 'b -> 'a t -> 'a t -> 'b

