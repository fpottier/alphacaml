{

  open Parser

  exception Lexical_error of int

}

rule main = parse
| [' ' '\009' '\012' '\r' '\n' ]+
    { main lexbuf }
| "="
    { EQUAL }
| "+"
    { PLUS }
| "fail"
    { FAIL }
| (['a'-'z' '_'] ['A'-'Z' 'a'-'z' '_' '0'-'9' '\'']*) as str
    { VAR str }
| (['0'-'9']+) as str
    { CONST (int_of_string str) }
| eof
    { EOF }
| _ 
    { raise (Lexical_error (Lexing.lexeme_start lexbuf)) }
