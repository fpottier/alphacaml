close (
  (fake z depends on y in mixin
    val x
    val y
    val z = x
  end)
  +
  mixin
    val x = mix end
    val y = mix end
  end
)
