/* Examples for testing */

lambda x:Top. x;

(lambda x:Top. x) (lambda x:Top. x);

(lambda x:Top->Top. x) (lambda x:Top. x);

(lambda r:{x:Top->Top}. r.x r.x) {x=lambda z:Top.z, y=lambda z:Top.z}; 

A ;

lambda x:A. x;

lambda X. lambda x:X. x; 

(lambda X. lambda x:X. x) [All X.X->X]; 

lambda X<:Top->Top. lambda x:X. x x; 

lambda x:Top. x x; /* ill-typed */

a : A;

(lambda X. lambda Y<:X. lambda Z. lambda f:X -> Z. lambda y:Y. f y) [A] [A] [A] (lambda x:A. x) a;

