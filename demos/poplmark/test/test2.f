T;

Stack;

lambda stacks : {
  empty: Stack,
  push: T -> Stack -> Stack,
  pop: Stack -> Stack
}.
lambda x : T.
  let { empty = s : Stack,
        push = push : T -> Stack -> Stack,
        pop = pop : Stack -> Stack } = stacks in
  let s : Stack = push x s in
  let s : Stack = pop s in
  s

;

