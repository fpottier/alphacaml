open AlphaLib.Signatures

module Identifier : Identifier with type t = string

module Toolbox : Toolbox with type Atom.identifier = Identifier.t

type subst =
    Toolbox.Subst.subst

type atoms =
    Toolbox.AtomSet.atoms

type export =
    Toolbox.Export.env

module type Data = sig

  type var

  type 'a abs

  type 'a abstractable

  type expression =
    | EVar of var
    | ELambda of binding abs
    | EApp of expression * expression

  and binding =
      var * expression

end

module Internal : sig

  include Data with type var = Toolbox.Atom.t
                and type 'a abs = 'a Toolbox.Abstraction.abs
                and type 'a abstractable = 'a Toolbox.Abstraction.abstractable

  val pack_binding: binding -> binding abs
  val unpack_binding: binding abs -> binding
  val expose_binding: binding abs -> binding
  val unpack2_binding: binding abs -> binding abs -> binding * binding

  class dictionaries : object

    method binding_dictionary: binding abstractable

  end

  class cmap : object ('self)

    inherit dictionaries

    method free_var: var map
    method bound_var: var map

    (* [abstraction] must define an abstraction transformer, and is
       provided with a content transformer. It gets an opportunity to
       update [self] before calling the content transformer. It gets
       access to a dictionary for the content. *)

    method abstraction: 'a . ('self -> 'a abstractable) -> ('self -> 'a map) -> 'a abs map
    method outer: 'a . ('self -> 'a map) -> 'a map
    method inner: 'a . ('self -> 'a map) -> 'a map

    method expression: expression -> expression
    method evar: expression -> var -> expression
    method elambda: expression -> binding abs -> expression
    method eapp: expression -> expression -> expression -> expression
    method binding: binding -> binding

  end

  class ['accu] cfold : object ('self)

    inherit dictionaries

    method free_var: ('accu, var) fold
    method bound_var: ('accu, var) fold
    method abstraction: 'a . ('self -> 'a abstractable) -> ('self -> ('accu, 'a) fold) -> ('accu, 'a abs) fold
    method outer: 'a . ('self -> ('accu, 'a) fold) -> ('accu, 'a) fold
    method inner: 'a . ('self -> ('accu, 'a) fold) -> ('accu, 'a) fold

    method expression: 'accu -> expression -> 'accu
    method evar: 'accu -> var -> 'accu
    method elambda: 'accu -> binding abs -> 'accu
    method eapp: 'accu -> expression -> expression -> 'accu
    method binding: 'accu -> binding -> 'accu

  end

  class ['accu] cfold2 : object ('self)

    inherit dictionaries

    method free_var: ('accu, var) fold2
    method bound_var: ('accu, var) fold2
    method abstraction: 'a . ('self -> 'a abstractable) -> ('self -> ('accu, 'a) fold2) -> ('accu, 'a abs) fold2
    method outer: 'a . ('self -> ('accu, 'a) fold2) -> ('accu, 'a) fold2
    method inner: 'a . ('self -> ('accu, 'a) fold2) -> ('accu, 'a) fold2

    method expression: 'accu -> expression -> expression -> 'accu
    method evar: 'accu -> var -> var -> 'accu
    method elambda: 'accu -> binding abs -> binding abs -> 'accu
    method eapp: 'accu -> expression -> expression -> expression -> expression -> 'accu
    method binding: 'accu -> binding -> binding -> 'accu

  end

  val subst_expression: subst -> expression map
  val subst_binding: subst -> subst -> binding map

  val f_expression: expression -> atoms
  val f_binding: binding -> atoms * atoms * atoms

  val aeq_expression: expression eq
  val aeq_binding: binding eq

end

module Raw : sig

  include Data with type var = Identifier.t
                and type 'a abs = 'a

end

val export_expression: export -> Internal.expression -> Raw.expression
val export_binding: export -> export -> Internal.binding -> Raw.binding

module Flat : sig

  include Data with type var = Toolbox.Atom.t
                and type 'a abs = 'a

end

val flatten_expression: Internal.expression -> Flat.expression
val flatten_binding: Internal.binding -> Flat.binding

val unflatten_expression: Flat.expression -> Internal.expression
val unflatten_binding: Flat.binding -> Internal.binding

