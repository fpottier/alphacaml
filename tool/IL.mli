(* Abstract syntax of the language used for code production. *)

type path =
  | PathVar of modulevar
  | PathDot of path * modulevar

and structure_item =
  | StructValDef of var * expr
  | StructTypeDefs of typedef list
  | StructModuleDef of modulevar * modulexpr
  | StructModuleTypeDef of moduletypevar * moduletypexpr
  | StructClassDef of virtuality * typevar list * classvar * classexpr
  | StructInclude of modulexpr
  | StructOpen of path
  | StructComment of string

and modulexpr =
  | MEPath of path
  | MEStruct of structure_item list
  | MEFunctor of modulevar * moduletypexpr * modulexpr
  | MEApply of modulexpr * modulexpr

and signature_item =
  | SigValDecl of var * typescheme
  | SigTypeDecls of typedef list
  | SigModuleDecl of modulevar * moduletypexpr
  | SigModuleTypeDecl of moduletypevar * moduletypexpr
(* TEMPORARY SigClassDecl *)
  | SigInclude of moduletypexpr
  | SigComment of comment
  | SigOpen of path

and moduletypexpr =
  | MTEPath of path
  | MTESig of signature_item list
  | MTEWith of moduletypexpr * typedef

and classexpr =
  | CPath of path
  | CObject of (var * typ option) option * class_item list
  | CFun of var list * classexpr
  | CApp of classexpr * expr list

and class_item =
  | CIMethod of privacy * virtuality * field * typescheme option * expr option
  | CIInherit of classexpr

and virtuality =
  | Virtual
  | NonVirtual

and privacy =
  | Private
  | Public

and typedef = {

    (* Name of the type. *)

    typename: typecon;

    (* Type parameters. This is a list of type variable names,
       without the leading quote, which will be added by the
       pretty-printer. *)

    typeparams: typevar list;

    (* Concrete type definition, [None] if abstract type. *)

    typerhs: typ ADT.rhs option;

  } 

and typ =
  
  (* Literal Objective Caml type. *)

  | TypLiteral of literal

  (* Type variable, without its leading quote. *)

  | TypVar of typevar

  (* Application of an algebraic data type constructor. *)

  | TypApp of path * typ list

  (* Anonymous tuple. *)

  | TypTuple of typ list

  (* Arrow type. *)

  | TypArrow of typ * typ

and typescheme = {

    (* Universal quantifiers, without leading quotes. *)

    quantifiers: typevar list;

    (* Body. *)

    body: typ;

} 

and expr =

  (* Wildcard. *)

  | EWildcard

  (* Variable. *)

  | EVar of path

  (* Function. *)

  | EFun of pattern list * expr

  (* Data. Tuples of length 1 are considered nonexistent, that is,
     [PTuple [p]] is considered the same pattern as [p]. *)

  | EData of path * expr list
  | ERecord of (field * expr) list
  | ETuple of expr list

  (* Literal Objective Caml value. *)

  | ELiteral of literal

  (* Function call. *)

  | EApp of expr * expr
  | EInfixApp of expr * operator * expr

  (* Method call *)

  | EMethodCall of expr * field

  (* Local definitions. *)

  | ELet of pattern * expr * expr

  (* Case analysis. *)

  | EMatch of expr * branch list
  | EIfThenElse of expr * expr * expr

  (* Raising exceptions. *)

  | ERaise of expr

  (* Exception analysis. *)

  | ETry of expr * branch list

  (* Record access. *)

  | ERecordAccess of expr * field

  (* Record update. *)

  | ERecordUpdate of expr * (field * expr) list

  (* Type constraint. *)

  | ETypeConstraint of expr * typ

  (* Object creation. *)

  | ENew of path

and pattern =
    expr

and branch = {

    (* Branch pattern. *)

    branchpat: pattern;

    (* Branch body. *)

    branchbody: expr;

  } 

and var =
    string

and typevar =
    string

and modulevar =
    string

and moduletypevar =
    string

and classvar =
    string

and typecon =
    string

and data =
    string

and field =
    string

and comment =
    string

and literal =
    string

and operator =
    string

