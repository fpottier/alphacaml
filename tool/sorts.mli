open IL

(* An ordered list of sorts. *)

type sort =
    string

val sorts: sort list

(* This module generates all of the code that depends on the finite
   set of sorts. *)

val defs : structure_item list

(* Helpers for constructing types and values. *)

val sortrec: typ list -> typ
val map: typ -> typ
val tagged: typ -> typ
val sortcon: string -> expr
