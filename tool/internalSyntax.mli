(* This is the internal syntax of type declarations. *)

type declaration =
  | DeclType of string list (* parameters *) * string (* type constructor *) * mode * factor ADT.rhs

and mode =
  | ModeExp
  | ModePat

and factor =
  | FAtom of string (* sort *)
  | FEscape of string (* Objective Caml type *)
  | FTypCon of modifier * string (* type constructor *) * string list (* parameters *)

and modifier =
  | MNone
  | MContainer of string (* container *)
  | MAbstraction
  | MInner
  | MOuter
  | MNeutral

