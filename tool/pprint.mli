(* ------------------------------------------------------------------------- *)

(* Basic combinators for building documents. *)

type document

val empty: document
val hardline: document
val char: char -> document
val substring: string -> int -> int -> document
val text: string -> document
val blank: int -> document
val (^^): document -> document -> document
val nest: int -> document -> document
val column: (int -> document) -> document
val nesting: (int -> document) -> document
val group: document -> document
val ifflat: document -> document -> document

(* ------------------------------------------------------------------------- *)

(* Low-level combinators for alignment and indentation. *)

val align: document -> document
val hang: int -> document -> document
val indent: int -> document -> document

(* ------------------------------------------------------------------------- *)

(* High-level combinators for building documents. *)

val line: document
val linebreak: document
val softline: document
val softbreak: document
val string: string -> document
val words: string -> document

val lparen: document
val rparen: document
val langle: document
val rangle: document
val lbrace: document
val rbrace: document
val lbracket: document
val rbracket: document
val squote: document
val dquote: document
val semi: document
val colon: document
val comma: document
val space: document
val dot: document
val sharp: document
val backslash: document
val equals: document

val squotes: document -> document
val dquotes: document -> document
val braces: document -> document
val parens: document -> document
val angles: document -> document
val brackets: document -> document

val fold: (document -> document -> document) -> document list -> document
val fold1: (document -> document -> document) -> document list -> document
val fold1map: (document -> document -> document) -> ('a -> document) -> 'a list -> document
val sepmap: document -> ('a -> document) -> 'a list -> document

val optional: ('a -> document) -> 'a option -> document
      
(* ------------------------------------------------------------------------- *)

(* A signature for document renderers. *)

module type RENDERER = sig
  
  (* Output channels. *)

  type channel

  (* [pretty rfrac width channel document] pretty-prints the document
     [document] to the output channel [channel]. The parameter [width] is the
     maximum number of characters per line. The parameter [rfrac] is the
     ribbon width, a fraction relative to [width]. The ribbon width is the
     maximum number of non-indentation characters per line. *)

  val pretty: float -> int -> channel -> document -> unit

  (* [compact channel document] prints the document [document] to the output
     channel [channel]. No indentation is used. All newline instructions are
     respected, that is, no groups are flattened. *)

  val compact: channel -> document -> unit

end

(* ------------------------------------------------------------------------- *)

(* Renderers to output channels and to memory buffers. *)

module Channel : RENDERER with type channel = out_channel

module Buffer : RENDERER with type channel = Buffer.t

