AlphaCaml is a tool that turns a so-called "binding specification" into an
Objective Caml compilation unit. A binding specification resembles an
algebraic data type declaration, but also includes information about names and
binding. AlphaCaml is meant to help writers of interpreters, compilers, or
other programs-that-manipulate-programs deal with alpha-conversion in a safe
and concise style.

AlphaCaml is described in [this paper](http://cambium.inria.fr/~fpottier/publis/fpottier-alphacaml.pdf).

AlphaCaml has not been updated since 2006.
