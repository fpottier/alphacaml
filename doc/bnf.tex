\section{Syntax of specifications}

% TEMPORARY appliquer la r�forme des containers

\newcommand{\bindsclause}{{\kw{binds} \nt{sort} \kw{, }$\ldots$ \kw{, }\nt{sort}}}
\newcommand{\factors}{{\nt{factor} \kw{*} $\ldots$ \kw{*} \nt{factor}}}

\begin{figure}[p]
\newcommand{\is}{& ${} ::= {}$ \\}
\begin{center}
\begin{tabular}{r@{}c@{}l}
%
\nt{specification} \is
&& \optional{\nt{prologue}} \nt{declaration} $\ldots$ \nt{declaration} \\
%
\nt{prologue} \is
&& \kw{[} \nt{arbitrary \ocaml directives} \kw{]} \\
%
\nt{declaration} \is
&& \kw{sort} \nt{sort} \\
&& \kw{type} \optional{\nt{typevars}} \nt{type} \optional{\bindsclause} \kw{=} \nt{body} \\
&& \kw{container} \nt{container} \kw{with} \nt{map} \kw{and} \nt{fold} \kw{and} \nt{fold2} \\
&& \kw{identifier module} \nt{module} \\
%
\nt{body} \is
&& \nt{branch} $\ldots$ \nt{branch} \\
&& \factors \\
&& \kw{\textbraceleft} \nt{label}\kw{ :} \nt{factor}\kw{;} $\ldots$ \kw{;} \nt{label}\kw{ :} \nt{factor} \kw{\textbraceright} \\
%
\nt{branch} \is
&& \kw{\textbar} \nt{data} \optional{\kw{of} \factors} \\
%
\nt{factor} \is
&& \kw{atom} \nt{sort} \\
&& \kw{[} \nt{arbitrary \ocaml type} \kw{]} \\
&& \optional{\nt{specifier}} \optional{\nt{typevars}} \nt{type} \optional{\nt{container}} \\
&& \kw{\textless} \optional{\nt{typevars}} \nt{type} \kw{\textgreater} \\
&& \kw{\textless (} \nt{type} \bindsclause \kw{)} \nt{body} \kw{\textgreater} \\
%
\nt{specifier} \is
&& \kw{inner} \\
&& \kw{outer} \\
&& \kw{neutral} \\
%
\nt{typevars} \is
&& \nt{typevar} \\
&& \kw{(} \nt{typevar}\kw{, }$\ldots$ \kw{, }\nt{typevar} \kw{)} \\
%
\nt{typevar} \is
&& \kw{'} \nt{lid} \\
%
\nt{sort, type, label} \is
&& \nt{lid} \\
%
\nt{data, module} \is
&& \nt{uid} \\
%
\nt{container}, \nt{map}, \nt{fold}, \nt{fold2} \is
&& \nt{qid} \\
\end{tabular}
\end{center}
\caption{Syntax of specifications}
\label{fig:syntax}
\end{figure}

\paragraph*{Notation}

Our terminal symbols are either literals, written in \kw{bold} face, or one of
\nt{lid}, \nt{uid}, and \nt{qid}. The terminal symbol \nt{lid} represents an
\ocaml identifier whose initial letter is lowercase, such as
\basic{beGentle}. The terminal symbol \nt{uid} represents an \ocaml identifier
whose initial letter is uppercase, such as \basic{Zero}. The terminal symbol
\nt{qid} represents an identifier whose initial letter is lowercase, possibly
qualified with a module path, such as \basic{List.map}. These three lexical
categories are defined under the names \nt{lowercase-ident},
\nt{capitalized-ident}, and \nt{value-path} in \ocaml's manual
(\href{http://caml.inria.fr/pub/docs/manual-ocaml/manual009.html}{lexical
conventions} and
\href{http://caml.inria.fr/pub/docs/manual-ocaml/manual011.html}{names}).

Non-terminal symbols are written in \nt{italics}. The definition of a
non-terminal symbol \nt{nt} begins with \nt{nt}${} ::= {}$ and goes on with a
series of valid expansions for this symbol, each of which appears on a
separate line. Each expansion is, to a first approximation, a sequence of
terminal and non-terminal symbols. Square brackets \optional{$\cdot$} delimit
an optional sub-sequence. Ellipses $\ldots$ are used to indicate repetitions
of a sub-sequence. Repetitions may or may not involve a delimiter. Although
this notation is ambiguous, our syntax is simple enough that no difficulty
should arise.

The syntax of specification (\mla) files appears in \fref{fig:syntax}.
We now briefly explain each production.

\paragraph*{Specification}

A specification consists of an optional prologue, followed by
declarations. The order in which the declarations appear is irrelevant: all
declarations are considered mutually recursive.

\paragraph*{Prologue}

A prologue is a piece of \ocaml text, delimited with square brackets. The
prologue is copied verbatim to \emph{both} of the generated files---that is,
to the \ml file and to the \mli file---so it should make sense in both
contexts. The prologue usually consists of \kw{open} directives and of type
definitions.

\paragraph*{Declaration}

A declaration is a \emph{sort declaration}, a \emph{type declaration}, a
\emph{container declaration}, or an \emph{identifier module declaration}.

\mypoint{Sort declarations} A sort declaration introduces a new sort of
atoms. It is possible to declare as many sorts as desired. Each sort
declaration gives rise, in the generated code, to a distinct module. For
instance, declaring ``\kw{sort} \basic{termvar}'' gives rise to a module
named \basic{Termvar}; declaring ``\kw{sort} \basic{typevar}'' gives rise to
a module named \basic{Typevar}. Both \basic{Termvar} and \basic{Typevar} have
signature
%
\begin{quote}
\basic{AlphaLib.Signatures.Atom} \kw{with type} \basic{identifier} = \basic{Identifier.t}
\end{quote}
%
The module type \basic{Atom} is defined in \sigs. The module
\basic{Identifier} is defined as part of the generated code;
its identity can be controlled via an identifier module
declaration. Note that \basic{Termvar.Atom.t} and \basic{Typevar.Atom.t}
are distinct abstract types: that is, atoms of distinct sorts
cannot be mixed.

\mypoint{Type declarations} A type declaration introduces a new type. It is
optionally parameterized by a sequence of \ocaml type variables. These
variables, if present, are allowed to appear inside the body of the
declaration. There is, however, a restriction: all occurrences of a type
\basic{t} in the specification should carry the \emph{same} sequence of
parameters, that is, the same type variables, in the same order.

The \kw{binds} clause is optional. If no clause is present, the type that is
being declared is considered an expression type; otherwise, it is considered a
pattern type. A \kw{binds} clause mentions a set of sorts, which are
considered bound by the pattern.

Each type declaration gives rise, in the generated code, to \emph{two} type
declarations, one of which lies at toplevel, and one of which lies inside
the \basic{Raw} sub-module. Thus, declaring a type \basic{t} gives rise to
two types named \basic{t} and \basic{Raw.t}.

\mypoint{Container declarations} A container declaration introduces a new
container. A container \basic{t} is an \ocaml type constructor with one
parameter. It must represent a pure (that is, persistent) data structure with
the semantics of a container: that is, values of type $\alpha\,\basic{t}$ must
represent collections of values of type $\alpha$. It must come with
\basic{map}, \basic{fold}, and \basic{fold2} functions, whose types must be
%
$$\begin{array}{rcl}
\basic{map} & : & \forall\alpha\beta.(\alpha\rightarrow\beta)\rightarrow\alpha\,\basic{t}\rightarrow\beta\,\basic{t} \\
\basic{fold} & : & \forall\alpha\beta.
(\alpha\rightarrow\beta\rightarrow\alpha)\rightarrow\alpha\rightarrow\beta\,\basic{t}\rightarrow\alpha \\
\basic{fold2} & : & \forall\alpha\beta.
(\alpha\rightarrow\beta\rightarrow\beta\rightarrow\alpha)
\rightarrow\alpha\rightarrow\beta\,\basic{t}\rightarrow\beta\,\basic{t}\rightarrow\alpha
  \end{array}$$
%
The semantics of the \basic{map} and \basic{fold} operations is standard and
will not be repeated here. \basic{fold2} should iterate over two containers
simultaneously, in a synchronized manner. It should fail, by raising the
exception \basic{Invalid\_argument}, if the two containers have different
structure -- for instance, in the case of lists, if the two lists have
distinct lengths.

The names of these three functions must be supplied as part of the declaration.
The containers \basic{list} and \basic{option} are predefined (and cannot be
redefined).

\mypoint{Identifier module declarations} An identifier module declaration
specifies the name of an \ocaml module, whose signature must be
\basic{AlphaLib.Signatures.Identifier}. This module is adopted as the
definition of identifiers. It is provided as a parameter to the functor
\basic{AlphaLib.Atom.Make} in order to produce implementations of atoms. At
most one identifier module declaration can appear in a specification. If none
appears, then the default implementation \basic{AlphaLib.Atom.String} is used.
In this default implementation, identifiers are strings, and fresh identifiers
are generated, when needed, by appending the decimal representation of an
integer counter.

\paragraph*{Body}

The body (that is, the right-hand side) of a type declaration can consist of a
\emph{sum type}, a \emph{tuple type}, or a \emph{record type}. A sum type
consists of a list of branches. A tuple type consists of a list of factors. A
record type consists of a list of factors, each of which carries a label.

\paragraph*{Branch}

Each branch in a sum type consists of a data constructor, optionally followed
by a list of factors.

\paragraph*{Factor}

A factor is an \emph{atom type}, a \emph{foreign type}, a \emph{type
reference}, or an \emph{abstraction type}. Not all factors are allowed in all
contexts: some factors are valid only in the declaration of an expression
type, while others are valid only in the declaration of a pattern type.

\mypoint{Atom types} An atom type consists of the keyword \kw{atom}, followed
by a sort \nt{sort}. It can appear both within expression types and within
pattern types. Within an expression type, it is interpreted as a referring
occurrence. Within a pattern type, it is interpreted as a binding occurrence;
furthermore, in that case, the sort \nt{sort} must be mentioned in the
\kw{binds} clause for that type.
%
% This implies, in fact, that the set of sorts that appears in the \kw{binds}
% clause is redundant: it could be reconstructed, if necessary. This
% redundancy is intentional.

\mypoint{Foreign types} A foreign type is an arbitrary \ocaml type expression,
enclosed within square brackets. This expression can refer to any of the
\ocaml type variables currently in scope. For purposes of \acon, values of
foreign type are ignored entirely: that is, they are considered not to contain
any binding or referring occurrences of atoms.
%
% They may contain atoms, but these atoms are ignored.
%
Values that contain modifiable state, or whose structure is not known, because
it is represented by a type variable, are typically to be considered foreign.
Foreign types are valid within expression and pattern types.

\mypoint{Type references} A type reference primarily consists of (the name of)
a type, which must be defined elsewhere in the specification. It is optionally
preceded with a sequence of type variables, and optionally followed by a
container. Type references can appear inside expression and pattern types.
When within an expression type, no specifier must be given, and the reference
must be again to an expression type. When within a pattern type, if a
specifier is given, then the reference must be to an expression type: that is,
specifiers can be thought of as end-of-abstraction marks. Otherwise, it must
be again to a pattern type.

\mypoint{Abstraction types} Abstraction types are valid within expression
types only. There are two syntactic forms. In the simpler form, inside the
angle brackets is (the name of) a type, which must be defined elsewhere in the
specification and must be a pattern type. It is optionally preceded with a
sequence of type variables.

We stress that the contents of an abstraction must be a type \emph{name}: it
cannot be, for instance, an anonymous product of factors. This is because a
name is needed for the generated functions that allow creating and opening
this abstraction.

The inconvenience of having to refer to a type name is somewhat relieved
by the second, more elaborate form of abstraction types. In that form, a
pattern type is introduced on the fly, and becomes the body of the abstraction.
In other words, the definition of a pattern type is inlined into the
abstraction. For instance, the abstraction type
%
\begin{quote}
\kw{\textless(} \nt{lambda} \kw{binds} \nt{var} \kw{)} \kw{atom} \nt{var} \kw{*} \kw{inner} \nt{term} \kw{\textgreater}
\end{quote}
%
is syntactic sugar for the abstraction type
%
\begin{quote}
\kw{\textless} \nt{lambda} \kw{\textgreater}
\end{quote}
%
together with the type declaration
%
\begin{quote}
\kw{type} \nt{lambda} \kw{binds} \nt{var} \kw{=} \kw{atom} \nt{var} \kw{*} \kw{inner} \nt{term}
\end{quote}

\paragraph*{Specifier} A specifier precedes a reference to an expression type
within a pattern type. In other words, a specifier marks the end of an
abstraction. If the specifier is \kw{inner}, then the expression is considered
as lying \emph{inside} the scope of the abstraction. If it is \kw{outer}, then
the expression is considered as lying \emph{outside} the scope of the
abstraction. When the expression contains, directly or indirectly, no atoms of
the sorts bound by the abstraction, then it makes no difference whether it
lies inside or outside the scope of the abstraction. In that case, and only in
that case, the \emph{neutral} specifier must be used. In other words, use of
the \emph{neutral} specifier is only permitted, and is required, in situations
where \kw{inner} and \kw{outer} would have the same meaning.

