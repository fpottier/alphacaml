% $Header: /home/yquem/cristal/fpottier/cvs/fresh/doc/main.tex,v 1.16 2006/09/26 07:57:01 fpottier Exp $
%
% Document: 
% Creation: 
% Author: Fran�ois Pottier.
%
\def\true{true}
\let\fpacm\true
\documentclass[onecolumn,11pt,nocopyrightspace]{sigplanconf}
\usepackage{amstext}
\usepackage[OT1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{listings} % Version 1.3 required.
\usepackage{type1cm}
\usepackage{xspace}
\usepackage{mymacros}
\usepackage[noweb]{ocamlweb}
\usepackage{fppdf}
\input{../paper/macros}
\input{macros}
\input{version}

% TEMPORARY supprimer listings au profit d'ocamlweb? exige de mettre en gras les mots-clef suppl�mentaires

% TEMPORARY documenter toutes les raisons qui peuvent mener � un message d'erreur
% e.g. patterns that refer to each other must have identical "binds" clauses

% ---------------------------------------------------------------------------------------------------------------------
% Headings.

\title{\ac Reference Manual\\\normalsize (version \acversion)}

\begin{document}

\authorinfo{Fran�ois Pottier}
	   {INRIA}
	   {Francois.Pottier@inria.fr}

\maketitle

% ---------------------------------------------------------------------------------------------------------------------

\section{Foreword}

\ac (pronounced: ``alphaCaml'') is a tool that accepts a \emph{binding
specification} and turns it into \ocaml type definitions and code. The
generated code relies on a library known as
\basic{alphaLib}. Roughly speaking, a binding specification is a definition
of one or several algebraic data types, enriched with information about
\emph{names} (henceforth referred to as \emph{atoms}) and \emph{binding}.
This information gives rise to a notion
of \aeq over the values that inhabit these types. The code produced by \ac is
intended to help deal with this notion in a safe and concise style.

This document is a reference manual. It is not a discussion of the problems
raised by \aeq and of the various ways in which they can be addressed. Neither
is it a tutorial introduction to \ac. These topics are covered in a separate
paper~\cite{pottier-alphacaml}, which should be read first. Having a look at
the demos that are shipped with \ac is also recommended.

% ---------------------------------------------------------------------------------------------------------------------

\section{Terminology}

\ac values are split into \emph{expressions} and \emph{patterns}. Expressions are terms,
that is, abstract syntax trees. Expressions contain \emph{abstractions} inside
which atoms can be bound. Inside abstractions are \emph{patterns}. Patterns
are also terms, but are slightly different. They cannot contain abstractions;
that is, abstractions cannot be nested. Patterns can contain expressions,
preceded with a \emph{specifier} that tells whether the expression lies inside
or outside the \emph{scope} of the enclosing abstraction. The distinction
between expressions and patterns is reflected in specifications, where each
type is explicitly marked as an \emph{expression type} or a \emph{pattern
type}.

Not all occurrences of an atom play the same role. For instance, consider the
$\lambda$-term $\at\,(\lambda \at.\at)$. The central occurrence of the atom
$\at$ is meant to \emph{bind} $\at$ in the body of the $\lambda$-abstraction:
it is a \emph{binding occurrence}. The first and last occurrences of $\at$, on
the other hand, are meant to
\emph{refer} to a previous binding occurrence of $\at$: they are \emph{referring occurrences}.

Both expressions and patterns can contain atoms, but they are interpreted
differently. Occurrences of atoms that lie (directly) inside an expression are
interpreted as referring occurrences, while occurrences of atoms that lie
(directly) inside a pattern are interpreted as binding occurrences. In fact,
this is a good way of summarizing the distinction between expressions and
patterns.

It is common to need several distinct \emph{sorts} of atoms---for instance,
the abstract syntax of a typed programming language typically involves both
term variables and type variables, which are separate. \ac's specification
language allows dealing with multiple sorts, as long as there is only a finite
number of them.

% ---------------------------------------------------------------------------------------------------------------------

\section{Usage}

By convention, binding specifications are stored in files whose name ends with
\mla. Out of such a file, \ac produces an \ocaml compilation unit, that is, a pair of an
\ml file and an \mli file. Both rely on the \basic{alphaLib} library.  Please
have a look at the demos if you need help writing a Makefile in order to
automate the compilation process. Read the generated \mli file---it is meant
to be instructive. In order to understand how atoms are implemented and what
operations they support, consult the definition of the sub-module \sigs
(Appendix~\ref{sigs}).

% ---------------------------------------------------------------------------------------------------------------------

\input{bnf}

% ---------------------------------------------------------------------------------------------------------------------

\section{Generated Code}

The compilation unit produced by \ac out of a specification file contains a
number of \ocaml module, type, value, and class definitions.

\paragraph*{Modules}

For each atom sort declared in the specification, an ``atom'' module is
generated. Each such module is produced by applying the functor
\basic{AlphaLib.Atom.Make} to the \basic{Identifier} module. (The
identity of the \basic{Identifier} module can be controlled via an
\kw{identifier module} declaration.) As a result, every ``atom'' module
has signature
%
\begin{quote}
\basic{AlphaLib.Signatures.Atom} \kw{with type} \basic{identifier} = \basic{Identifier.t}
\end{quote}
%
Each ``atom'' module defines an abstract type of atoms and provides a number
of operations involving atoms, sets and maps over atoms, maps of atoms to
identifiers, and substitutions of atoms for atoms. The definition of the
signature \basic{AlphaLib.Signatures.Atom} can be found in Appendix~\ref{sigs}.

\paragraph*{Types}

For each type \basic{t} declared in the specification, \emph{three} type
definitions are produced, respectively called the \emph{raw}, \emph{internal},
and \emph{flat} versions.

The \emph{raw} version, named \basic{Raw.t}, is concrete: atoms are
identifiers, and abstractions are transparent---that is, the angle brackets
are erased. The raw version is intended for use by parsers and
pretty-printers.

The \emph{internal} version, named \basic{t}, is abstract: atoms are abstract
values of the appropriate ``atom'' module, and abstractions are opaque---that
is, they are represented by abstract types. More precisely, in the internal
version, for each pattern type \basic{u} that appears within the body of an
abstraction, \emph{two} type definitions are produced: one, named
\basic{opaque\_u}, is abstract, while the other, named \basic{u}, is
transparent. Functions (\basic{create\_u} and \basic{open\_u}) are provided to
convert back and forth between the two forms. The internal version is
recommended for most uses.

The \emph{flat} version, named \basic{Flat.t}, offers abstract atoms, like the
internal version, and transparent abstractions. One can think of it as a
variant of the internal version in which all abstractions have been opened,
once and for all, while guaranteeing that ``all bound names are
distinct''. The flat version is intended for use in some applications where
initial distinctness of bound names is sufficient, so that ``working up to
\acon'' is not really required. In such cases, it can be more
convenient than the internal version.

Functions are provided to convert back and forth between the raw and internal
versions (see \basic{import\_t} and \basic{export\_t}) and between the
internal and flat versions (see \basic{flatten\_t} and \basic{unflatten\_t}).

\paragraph*{Values}

For each expression type \basic{t}, two functions \basic{import\_t} and
\basic{export\_t} allow converting between raw and internal forms, that is, between \basic{Raw.t} and \basic{t}.
The function \basic{import\_t} expects one or several mappings of identifiers to atoms---one mapping
per relevant sort.  Conversely, \basic{export\_t} expects one or several
mappings of atoms to identifiers.  These mappings are required when the terms
to be converted contain free atoms or free identifiers. When converting closed
terms, empty mappings can be provided.
% import\_t and export\_t could be made available when t is a pattern type,
% but this has not been deemed useful so far.

For each expression type \basic{t}, two functions \basic{flatten\_t} and
\basic{unflatten\_t} allow converting between internal and flat forms, that is,
between \basic{t} and \basic{Flat.t}. The function \basic{flatten\_t}
recursively traverses its argument, opening every abstraction that it
encounters. The result is a ``flat'' term, that is, a term within which all
abstractions are transparent, and all bound atoms are guaranteed to be
distinct. (Of course, it is possible to manually construct ``flat'' terms
where \emph{not} all bound atoms are distinct---it is up to you to be
careful.) Similarly, the function \basic{unflatten\_t} recursively traverses
its argument, re-creating opaque abstractions where needed.

For each type \basic{t}, a function \basic{equal\_t} offers an equality test,
up to \acon. That is, \basic{equal\_t} accepts two terms of type \basic{t} and
tells whether they are $\alpha$-variants of one another.

For each type \basic{t}, a function \basic{subst\_t} allows applying one or
several substitutions of atoms for atoms to a term of type \basic{t}---one substitution per
relevant sort.

For each expression type \basic{t}, a function \basic{free\_t} returns the sets
of atoms that appear free in a term---again, one set per relevant sort. For
each pattern type \basic{u}, a function \basic{bound\_u} returns the sets of
atoms that appear in a binding position in a term. A more complex function
\basic{bound\_free\_u} returns the sets of atoms that appear in a binding position,
free in inner scope, or free in outer scope.

For each pattern type \basic{u} that appears within the body of an
abstraction, two functions \basic{create\_u} and \basic{open\_u} are produced,
which convert back and forth between \basic{u} and
\basic{opaque\_u}. The function \basic{create\_u} has no runtime effect, while \basic{open\_u}
``freshens'' all bound atoms: that is, it replaces them with fresh atoms. This
enforces the informal convention that ``bound names and free names
must be chosen distinct''.

For each pattern type \basic{u} that appears within the body of an
abstraction, a function \basic{open2\_u} is produced, which allows opening
\emph{two} opaque abstractions at once, while automatically ensuring that
\emph{their bound atoms coincide}. Of course, this is possible only if the
two values of type \basic{opaque\_u} that are supplied to \basic{open2\_u}
have identical structure, that is, if \emph{their bound atoms are initially in
a bijection}. If no such bijection exists, \basic{open2\_u} fails and raises
the exception \basic{Open2}. In the simple case where the pattern type
\basic{u} binds exactly one atom, \basic{open2\_u} never fails---its use
is equivalent, in that case, to two independent calls to \basic{open\_u},
followed with an explicit renaming operation. In more complex cases, the
definition of \basic{u} could involve products or sums, so that \basic{u}
binds multiple atoms or a variable number of atoms. In these cases,
\basic{open2\_u} can in general fail.

\paragraph*{Classes}

In order to help modularly define transformations and traversals over terms,
\ac produces two classes, named \basic{map} and \basic{fold}.

The class \basic{map} contains one method for every type, data constructor,
and record label in the specification. The method associated with type
\basic{t} has type $\basic{t} \rightarrow \basic{t}$. Its default
implementation returns a copy of its argument. The copy is created via
\emph{self}-calls to the relevant methods for copying sub-terms, copying
record fields and/or copying data constructor applications. The method
associated with a data constructor \basic{D} has the same type as
\basic{D}. Its default implementation applies \basic{D} to a copy of its
argument; again, the copy is obtained through appropriate \emph{self}-calls.
The method associated with a record field \basic{f} of type $\tau$ has type
$\tau \rightarrow \tau$. Its default
implementation returns a copy of its argument, again created via appropriate
\emph{self}-calls.

If one were to
create an object of class \basic{map} via \basic{new map}, each of its methods
would behave as an identity function, whose argument is traversed and copied
without change. The point is that it is now easy to create a sub-class of
\basic{map} where one or several methods are overridden. This results in
application-specific behavior at certain nodes and default behavior at every
other node. For instance, capture-free substitution of terms for variables can
be defined in a few lines of code using this technique. This is illustrated in
\texttt{demos/poplmark/core.ml}.

The class \basic{fold} is very similar to \basic{map}, except terms are only
traversed, not copied. An accumulator is threaded through every call, that is,
accepted and returned by every method. The class is parametric in the type of
the accumulator.

% ---------------------------------------------------------------------------------------------------------------------

\section{Questions and Answers}

\question{How do I run experiments in the toplevel loop?}
Create a specification file \texttt{foo.mla} and compile it, using
\ac and \ocaml, so as to create \texttt{foo.cmi} and \texttt{foo.cmo}.
Then, you
can exploit the toplevel loop by entering the following directives:
%
\begin{verbatim}
#use "topfind";;
#require "alphaLib";;
#load "foo.cmo";;
\end{verbatim}
%
If (through its prologue) your specification depends on other \ocaml
modules, you must load them before attempting to load \texttt{foo.cmo}.

If you want to check which operations are available over atoms, type
%
\begin{verbatim}
module V = Foo.Var;;
\end{verbatim}
%
Assuming your specification contains the declaration ``\kw{sort} \nt{var}'',
this causes the signature of the corresponding ``atom'' module to be
displayed.

\question{What about cyclic terms?} Abstract syntax trees are not meant
to be cyclic. When using \ac, the only way of creating cyclic terms is
to exploit \ocaml's liberal \kw{let rec} construct. Don't do it.

\question{What about sharing?} If your terms happen to
have shared sub-terms, this is fine, but all sharing will be lost
when the terms are copied---that is, when they are freshened,
converted, renamed, etc.

\question{What about marshaling?} Marshaling and unmarshaling of
terms in internal form via
\basic{output\_value} and \basic{input\_value} is fine, as long as
the terms are \emph{closed}, that is, have no free atoms. Marshaling a term
that contains free atoms makes no sense, because the identity of an atom is
not preserved across runtime sessions. That is, two conceptually distinct
atoms that originate in different sessions could accidentally happen to
receive the same identity.

If you must marshal an open term, then you can do so, in an indirect way, by
closing it. You could, for instance, enclose it within a toplevel abstraction,
where each formerly free atom becomes bound and is explicitly paired with a
non-$\alpha$-varying label of your own making, such as a string or an
integer. Because the atom becomes bound, its identity vanishes, but
information about its meaning can be encoded in the label.

\begin{figure}
\lstinputlisting{../test/good/annot.mla}
\caption{Annotating terms with mutable information}
\label{fig:annot}
\end{figure}

\question{How do I annotate nodes with mutable information?} The trick
is to use an \ocaml \kw{ref} cell inside square brackets. It is even
possible to abstract over the type of the desired information, by
introducing a type parameter \basic{'a}. A sample specification that
uses this technique appears in \fref{fig:annot}.

\question{My lexer accepts identifiers that contain underscores and
numerals. Wouldn't that interact badly with the \basic{basename} and
\basic{combine} functions of the default \basic{Identifier} module
(Appendix~\ref{sigs})?}
%
No, this is fine. An identifier such as ``\basic{x\_\_3}'' is turned by the
\basic{import} functions into an atom of basename ``\basic{x}''. When this
atom is later supplied to an \basic{export} function, we obtain an identifier
that begins with ``\basic{x}'', followed by ``\_\_'', followed by whatever
integer is required to make the whole identifier unique. The result could be
just ``\basic{x}'' or perhaps ``\basic{x\_\_24}''.

In other words, dropping the ``\basic{\_\_3}''in ``\basic{x\_\_3}'' when
computing an atom's basename doesn't affect the fact that the atom is created
unique.  Furthermore, distinct atoms are mapped to distinct identifiers by the
\basic{export} functions. The basename mechanism is supposed to help these functions produce
suggestive identifiers, but does not affect their soundness.

In fact, it would be possible to produce an implementation of the
\basic{Identifier} signature (Appendix~\ref{sigs}) where \basic{basename} is a
constant function, that is, where no basename information is recorded. In that
case, the identifiers produced by the \basic{export} functions would be
isomorphic to integers. The composition of \basic{import} and \basic{export}
would then act as a ``name mangler''. This could be useful when trying to
obfuscate code!

% ---------------------------------------------------------------------------------------------------------------------

\clearpage
\appendix
\input{signatures}

% ---------------------------------------------------------------------------------------------------------------------
% Bibliography.

% TEMPORARY ref. biblio � mettre � jour
\bibliographystyle{plain}
\bibliography{local}

\end{document}
