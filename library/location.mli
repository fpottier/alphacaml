(* This module offers some facilities for dealing with source code
   locations. *)

type location =
    Lexing.position * Lexing.position

(* [current] reads and returns the ``current'' location. *)

val current: unit -> location

(* [under loc f x] temporarily changes the ``current'' location
   to [loc] while evaluating the application [f x]. Its result
   is the result of [f x]. The ``current'' location reverts to
   its previous value after [f x] is evaluated. *)

val under: location -> ('a -> 'b) -> 'a -> 'b

