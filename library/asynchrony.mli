(* This module provides the exception [Asynchrony]. *)

exception Asynchrony

val asynchrony: exn

val wrap: (unit -> 'a -> 'a -> unit) -> 'a -> 'a -> bool

