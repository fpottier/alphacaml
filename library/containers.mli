open Signatures

(* This module defines the built-in containers. *)

module List : Container with type 'a t = 'a list

module Option : Container with type 'a t = 'a option

type 'a decoration =
    Location.location * 'a

(* [map], [self_map], and [fold] use [Location.under] to temporarily
   update the current location. [fold2] does not, because that would
   require an arbitrary choice between two locations. *)

module Decoration : Container with type 'a t = 'a decoration

