open Signatures

(* An implementation of identifiers as strings. [basename] chops off
   all trailing digits, while [combine] appends a representation of
   the integer salt grain to the basename. *)

module String : Identifier with type t = string

(* An implementation of atoms, parameterized by an implementation of
   identifiers and over the number of distinct atom sorts that are
   desired. *)

module Make
    (Identifier : Identifier)
    (Sorts : sig val n: int end)
: Toolbox with type Atom.identifier = Identifier.t

